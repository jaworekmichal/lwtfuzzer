package pl.michaljaworek.learing.lwtfuzzer;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.StreamSupport;

import org.apache.log4j.Logger;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.DomNode;
import com.gargoylesoftware.htmlunit.html.HtmlAnchor;
import com.gargoylesoftware.htmlunit.html.HtmlForm;
import com.gargoylesoftware.htmlunit.html.HtmlInput;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlTextArea;
import com.gargoylesoftware.htmlunit.html.HtmlTextInput;

public class ClickingScenarios {

	private static final String TEXT_FORM_NAME = "TxText";
	private static final String TEXT_TITLE_FORM_NAME = "TxTitle";
	static Logger LOG = Logger.getLogger(ClickingScenarios.class.getName());
	private static int testsNumber = 0;
	private String rootUrl;

	public ClickingScenarios(String rootUrl) {
		this.rootUrl = rootUrl;
	}

	public void executeTextScenario(long numberOfIterations,
			int increaseTitleLenghtBy, int increateTextLengtBy) {
		testsNumber++;
		int titleLenght = 1;
		int textLenght = 1;

		logTestStart(increaseTitleLenghtBy, increateTextLengtBy);

		for (long i = 1; i <= numberOfIterations; i++) {
			try (WebClient webClient = new WebClient()) {
				executeTest(titleLenght, textLenght, i, webClient);
			} catch (Exception e) {
				logError(i, e, titleLenght, textLenght);
				break;
			} finally {
				titleLenght += increaseTitleLenghtBy;
				textLenght += increateTextLengtBy;
			}
		}
	}

	private void executeTest(int titleLenght, int textLenght, long i,
			WebClient webClient) throws IOException, MalformedURLException,
			Exception {
		HtmlPage page = openNewTextPage(i, webClient);
		HtmlForm form = page.getForms().get(0);

		String titleAdded = fillTitleField(form, i, titleLenght, textLenght);
		String textAdded = fillTextField(form, i, titleLenght, textLenght);
		String textId = saveText(titleLenght, textLenght, i, form);

		validateResults(i, webClient, titleAdded, textAdded, textId);
	}

	private void validateResults(long i, WebClient webClient,
			String titleAdded, String textAdded, String textId)
			throws Exception {
		validateHeaderPart(webClient, textId, titleAdded, i);
		validateTextPart(webClient, textId, textAdded, i);
	}

	private HtmlPage openNewTextPage(long i, WebClient webClient)
			throws IOException, MalformedURLException {
		HtmlPage page = webClient.getPage(rootUrl);

		HtmlAnchor myTextLink = page.getAnchorByText("My Texts");
		page = myTextLink.click();
		LOG.debug(String.format("Test %d, iteration %d: %s", testsNumber, i,
				"My text page opened"));

		page = page.getAnchorByHref("/lwtDev/edit_texts.php?new=1").click();
		return page;
	}

	private void logTestStart(int increaseTitleLenghtBy, int increateTextLengtBy) {
		LOG.debug(String
				.format("####################################################################\n"
						+ "Text scenario number %d started with parameters: increaseTitleLenghtBy %d, increateTextLengtBy %d",
						testsNumber, increaseTitleLenghtBy, increateTextLengtBy));
	}

	private void logError(long i, Exception e, int titleLenght, int textLenght) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		e.printStackTrace(pw);
		LOG.error(String.format(
				"Test %d, iteration %d (titleLenght: %d, textLenght: %d): %s",
				testsNumber, i, titleLenght, textLenght,
				"Execution interupted by exception: " + sw.toString()));
	}

	private String saveText(int titleLenght, int textLenght, long i,
			HtmlForm form) throws IOException {
		HtmlPage page;
		HtmlInput saveAndOpenButton = form.getInputByValue("Save and Open");
		page = saveAndOpenButton.click();
		String id = getTextIdFromUrl(page);
		LOG.debug(String.format(
				"Test %d, iteration %d (titleLenght: %d, textLenght: %d): %s",
				testsNumber, i, titleLenght, textLenght, "Text saved with id: "
						+ id));
		return id;
	}

	private String fillTextField(HtmlForm form, long i, int titleLenght,
			int textLenght) {
		HtmlTextArea textArea = form.getTextAreaByName(TEXT_FORM_NAME);
		String textAdded = CommonWords.getRandomParagraph(textLenght);
		textArea.setTextContent(textAdded);
		LOG.debug(String.format(
				"Test %d, iteration %d (titleLenght: %d, textLenght: %d): %s",
				testsNumber, i, titleLenght, textLenght, "Text set to value: "
						+ textAdded));
		return textAdded;
	}

	private String fillTitleField(HtmlForm form, long i, int titleLenght,
			int textLenght) {
		HtmlTextInput titleInput = form.getInputByName(TEXT_TITLE_FORM_NAME);
		String titleAdded = CommonWords.getRandomSentence(titleLenght);
		titleInput.setText(titleAdded);
		LOG.debug(String.format(
				"Test %d, iteration %d (titleLenght: %d, textLenght: %d): %s",
				testsNumber, i, titleLenght, textLenght, "Title set to value: "
						+ titleAdded));
		return titleAdded;
	}

	private boolean validateHeaderPart(WebClient webClient, String id,
			String titleAdded, long iterationNumber) throws Exception {
		HtmlPage page = webClient.getPage(this.rootUrl
				+ "do_text_header.php?text=" + id);
		String pageContent = page.asText();
		boolean isValid = pageContent.contains(titleAdded);
		if (isValid) {
			LOG.debug(String.format("Test %d, iteration %d: %s", testsNumber,
					iterationNumber, "Text title has been saved correctly"));
		} else {
			LOG.warn(String.format("Test %d, iteration %d: %s", testsNumber,
					iterationNumber,
					"Text title has not been saved correctly. Result was: "
							+ pageContent));
		}
		return isValid;
	}

	private boolean validateTextPart(WebClient webClient, String id,
			String textAdded, long iterationNumber) throws Exception {
		HtmlPage page = webClient.getPage(this.rootUrl
				+ "do_text_text.php?text=" + id);
		String pageContent = extractPageTextContent(page);
		boolean isValid = pageContent.contains(textAdded);

		if (isValid) {
			LOG.debug(String.format("Test %d, iteration %d: %s", testsNumber,
					iterationNumber, "Text has been saved correctly"));
		} else {
			LOG.warn(String.format("Test %d, iteration %d: %s", testsNumber,
					iterationNumber,
					"Text has not been saved correctly. Result was: "
							+ pageContent));
		}

		return isValid;
	}

	private String extractPageTextContent(HtmlPage page) {
		Iterable<DomNode> spans = page.getElementById("thetext")
				.getFirstChild().getChildren();
		StringBuilder resultBuilder = new StringBuilder();

		StreamSupport.stream(spans.spliterator(), false)//
				.map(span -> span.getTextContent())//
				.filter(word -> word.matches("[a-zA-Z ,.]*"))//
				.map(word -> word.equals("") ? " " : word)//
				.forEach(word -> resultBuilder.append(word));

		return resultBuilder.toString();
	}

	private String getTextIdFromUrl(HtmlPage page) {
		String url = page.getUrl().toString();
		Pattern pattern = Pattern.compile("text=([0-9]*)");
		Matcher matcher = pattern.matcher(url);
		matcher.find();
		String id = matcher.group(1);
		return id;
	}
}
