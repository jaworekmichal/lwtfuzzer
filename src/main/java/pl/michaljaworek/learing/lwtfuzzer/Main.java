package pl.michaljaworek.learing.lwtfuzzer;

import org.apache.log4j.Logger;

public class Main {

	static Logger LOG = Logger.getLogger(Main.class);

	private static final String ROOT_URL = "http://localhost/lwtDev/";

	public static void main(String[] args) throws Exception {
		LOG.debug("Program started");
		ClickingScenarios clickingScenarios = new ClickingScenarios(ROOT_URL);
		clickingScenarios.executeTextScenario(10, 10, 0);
		clickingScenarios.executeTextScenario(15, 0, 200);
		clickingScenarios.executeTextScenario(Long.MAX_VALUE, 0, 0);
	}

}
